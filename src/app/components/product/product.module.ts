import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { StoreModule } from '@ngrx/store';
import { key, reducer } from './product.reducer';



@NgModule({
  declarations: [ProductComponent, ListComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    StoreModule.forFeature(key, reducer),
  ]
})
export class ProductModule { }
