import { createAction, props } from '@ngrx/store';

export const TEST_REQUEST = '[Test request] test request';


export const testRequest = createAction(TEST_REQUEST, props<{ payload } | undefined>());