import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { testRequest } from './list.actions';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private store: Store<{}>) {
    console.log("list c")
  }

  ngOnInit(): void {
  }
  onSubmit() {
    this.store.dispatch(testRequest({ payload: 'test' }))
  }
}
