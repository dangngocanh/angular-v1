import { ActionReducerMap, combineReducers } from '@ngrx/store';
//import * as list from './list/list.reducer';

export const key = 'product';

export interface State {
    // [list.userListFeatureKey]: list.State;
}

const _reducer: ActionReducerMap<State> = {
    //[list.userListFeatureKey]: list.reducer,
};

const initialState = {
    // [list.userListFeatureKey]: list.initialState,
};

const metaReducer = combineReducers(_reducer, initialState);

export function reducer(state, action) {
    return metaReducer(state, action);
}
