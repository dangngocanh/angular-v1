export const APP_MENTU = [
    {
        title: 'Product',
        icon: 'shopping-cart-outline',
        children: [
            {
                title: 'All',
                link: '/products'
            },
            {
                title: 'Create New',
                link: '/products/create'
            },
            {
                title: 'Category',
                link: '/products/categories'
            }
        ]
    },
]